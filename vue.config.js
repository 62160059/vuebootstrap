module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160059/learn_bootstrap/'
    : '/'
}
